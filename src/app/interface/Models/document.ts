export interface Document{
  id : number ;
  type : DemandeType ,
  dateDemande : Date ,
  status : DemandeStatus
}
export enum DemandeType {
  BulletinPaie  = 'BULLETINDEPAIE' ,
  Attestation  = 'ATTESTATION'
}
export enum DemandeStatus {
  EnCours  = 'ENCOURS',
  Approuve  = 'APPROUVE'
}
