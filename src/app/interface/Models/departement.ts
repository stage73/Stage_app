import { UserToDepartment } from "./user";

export interface Departement {
    id:number;
    name:string;
    place:string;
}

export interface DepartementToEdit{
    id?: number;
    name:string;
    place:string;
  }

  export interface DepartementDetails{
    id?: number;
    name:string;
    place:string;
    users: UserToDepartment[] ;

  }

