import {Address} from "./address";
import {Company} from "./company";

export interface User {
  id?: number;
  name: string;
  username: string;
  email: string;
  address: Address;
  phone: string;
  userRole : string;
  company: Company;
}

export interface userToEdit{
  id?: number;
  firstName:string;
  lastName:string;
  dob:string;
  dateEntree:string;
  dateSortie:string;
  age : number;
  email :string;
  address :string;
  phoneNumber :string;
  userRole: string;
}

export interface UserToDepartment {
  id?: number;
  firstName: string;
  lastName: string;
  dob: string;
  age: number;
  email: string;
  phoneNumber: string;
  address:string;
  userRole: string;
}


