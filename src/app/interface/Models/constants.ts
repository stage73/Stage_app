export enum Patterns {
  phonePattern = "[0-9 ]{11}",
  namePattern = "[a-z]{2,15}"
}
