export interface Conge {
  id: number;
  dateDebut: string;
  dateFin: string;
  motif: string;
  duree: number;
  status: STATUS;
}

export enum STATUS {
  ACCEPTE = 'ACCEPTE',
  ENCOURS = 'ENCOURS',
  ANNULE = 'ANNULE',
}

export interface CongeToAdd {
  id?: number;
  dateDebut: Date;
  dateFin: Date;
  motif: string;
  status: STATUS;
}

export interface CongeToEdit {
  id?: number;
  dateDebut: Date;
  dateFin: Date;
  motif: string;
  status: STATUS;
}
