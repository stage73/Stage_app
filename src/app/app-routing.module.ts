import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserAddComponent } from './Core/Component/user-add/user-add.component';
import { MenuComponent } from './Core/Component/menu/menu.component';

import { DepartementFormComponent } from './Core/Component/departement-add/departement-form.component';
import { DepartementDetailsComponent } from './Core/Component/departement-details/departement-details.component';
import { UsersDisponibleComponent } from './Core/Component/users-disponible/users-disponible.component';
import { DepartementDetailsWrapperComponent } from './Core/Component/departement-details-wrapper/departement-details-wrapper.component';
import { DocumentAddComponent } from './Core/Component/document-add/document-add.component';
import { DocumentComponentComponent } from './Core/Component/document-table/document-component.component';
import { DocumentWrapperComponent } from './Core/Component/document-wrapper/document-wrapper.component';

const routes: Routes = [
  {path:'user-table', component:UsersWrapperComponent},
//  {path:'departement-table', component:DepartementComponent},
  {path:'departement', component:DepartementsWrapperComponent},
  {path:'departement-details/:id', component:DepartementDetailsWrapperComponent},
  {path:'user-add', component:UserAddComponent},
  {path:'departement-form', component:DepartementFormComponent},
  {path:'users-disponible', component:UsersDisponibleComponent},
  {path:'menu', component:MenuComponent},
  {path:'document', component : DocumentWrapperComponent},
  {path:'document-add', component : DocumentAddComponent}
 // {path:'departement-details/:id', component:DepartementDetailsComponent},


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
