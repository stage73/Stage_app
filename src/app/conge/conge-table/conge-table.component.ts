import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Conge, CongeToAdd, CongeToEdit } from 'src/app/interface/conge';
import { CongeService } from 'src/app/services/conge.service';
import { CongesCommunicationService } from 'src/app/services/conges-communication.service';
import { NotificationServiceService } from 'src/app/services/notification-service.service';

@Component({
  selector: 'app-conge-table',
  templateUrl: './conge-table.component.html',
  styleUrls: ['./conge-table.component.css']
})
export class CongeTableComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @Output() onClickEdit = new EventEmitter<any>();

  congeTable : Conge[] = [];
  dataSource: any = null;
  displayedDetailColumns = [
    'id',
    'dateDebut',
    'dateFin',
    'motif',
    'duree',
    'status',
    'update', 
    'delete'
  ];
  maDate = new Date();
  constructor(private congeService: CongeService ,
    private congeCommunicationService : CongesCommunicationService, private notificationService: NotificationServiceService) {
    
   }

  ngOnInit(): void {
    this.getConges();
    this.congeCommunicationService.getAddedConge().subscribe((addedConge:any) => {
      this.congeTable.push(addedConge);
      this.dataSource = new MatTableDataSource(this.congeTable);
    })
    this.congeCommunicationService
      .getEditedConge()
      .subscribe((editedConge: Conge) => {
        let index = this.congeTable.findIndex(
          (conge) => conge.id == editedConge.id
        );
        this.congeTable[index] = editedConge;
        this.dataSource = new MatTableDataSource(this.congeTable);
      });
  }

  getConges(){
    this.congeService.getConges().subscribe(
      (resp) => {
        this.congeTable = resp ;
        this.dataSource = new MatTableDataSource(this.congeTable);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {

      }
    )
  }

  FilterChange(event: Event) {
    const filvalue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filvalue;
  }

  onClick(element: any) {
    this.onClickEdit.emit(
      this.congeTable.filter((conge) => conge.id == element.id)[0]
    )
  }

  onDeleteConge(id: number) {
    this.congeService.deleteConge(id).subscribe((resp) => {
      this.getConges();
      this.notificationService.openNotification(
        'Conge deleted successfully',
        'Ok'
      );
    });
  }

}
