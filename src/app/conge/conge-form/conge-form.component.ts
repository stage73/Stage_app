import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDrawer } from '@angular/material/sidenav';
import { Conge, CongeToEdit } from 'src/app/interface/conge';
import { CongeService } from 'src/app/services/conge.service';
import { CongesCommunicationService } from 'src/app/services/conges-communication.service';
import { NotificationServiceService } from 'src/app/services/notification-service.service';

@Component({
  selector: 'app-conge-form',
  templateUrl: './conge-form.component.html',
  styleUrls: ['./conge-form.component.css'],
})
export class CongeFormComponent implements OnInit {
  @Output() onCancelEvent = new EventEmitter<boolean>();
  @Output() onSubmitCongeEvent = new EventEmitter<any>();

  createCongeForm!: FormGroup;
  congeToEdit!: CongeToEdit;

  constructor(
    private builder: FormBuilder,
    private congeService: CongeService,
    private congeCommunicationService: CongesCommunicationService,
    private notificationService: NotificationServiceService
  ) {}

  ngOnInit(): void {
    this.congeCommunicationService
      .getEditConge()
      .subscribe((congeToEdit: any) => {
        this.congeToEdit = congeToEdit;
        this.createCongeForm = this.builder.group({
          id: [this.congeToEdit.id],
          dateDebut: this.builder.control(
            this.congeToEdit.dateDebut,
            Validators.required
          ),
          dateFin: this.builder.control(
            this.congeToEdit.dateFin,
            Validators.required
          ),
          motif: this.builder.control(
            this.congeToEdit.motif,
            Validators.required
          ),
          status: this.builder.control(
            this.congeToEdit.status,
            Validators.required
          ),
        });
      });
    if (!this.congeToEdit) {
      this.createCongeForm = this.builder.group({
        id: [null],
        dateDebut: this.builder.control('', [Validators.required]),
        dateFin: this.builder.control('', [Validators.required]),
        motif: this.builder.control('', [Validators.required]),
        status: this.builder.control('', [Validators.required]),
      });
    }
  }

  onCancel() {
    this.onCancelEvent.emit(true);
    this.createCongeForm.reset();
  }

  addConge() {
    this.onSubmitCongeEvent.emit(this.createCongeForm.value);
    this.createCongeForm.reset();
    console.log('hheeh', this.createCongeForm.value);
  }
}
