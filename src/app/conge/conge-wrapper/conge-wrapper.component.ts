import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { Conge, CongeToAdd, CongeToEdit } from 'src/app/interface/conge';
import { CongeService } from 'src/app/services/conge.service';
import { CongesCommunicationService } from 'src/app/services/conges-communication.service';
import { NotificationServiceService } from 'src/app/services/notification-service.service';

@Component({
  selector: 'app-conge-wrapper',
  templateUrl: './conge-wrapper.component.html',
  styleUrls: ['./conge-wrapper.component.css']
})
export class CongeWrapperComponent implements OnInit {
  @ViewChild('drawer') public drawer!: MatDrawer;

  constructor(private congeService: CongeService ,
    private congeCommunicationService : CongesCommunicationService, private notificationService: NotificationServiceService) {
    
   }

   congeToEdit: any;

  ngOnInit(): void {
  }

  onSubmitConge($event: any) {
     if ($event?.id) {
      this.congeService
        .updateConge($event as CongeToEdit)
        .subscribe((editedConge: Conge) => {
          this.congeCommunicationService.setEditedConge(
            editedConge
          );
          this.drawer.toggle();
          this.notificationService.openNotification(
            'Conge updated succefully',
            'OK'
          );
        });
    } else {
    this.congeService.postConge($event as CongeToAdd)
    .subscribe((resp : CongeToAdd)=>{
      this.congeCommunicationService.setAddedConge(resp);
      this.drawer.toggle();
      this.notificationService.openNotification('Conge added succefully' ,   'OK');
    })
  }
}
  onEditClick(element: any) {
    this.congeToEdit = element;
    this.congeCommunicationService.setEditConge(
      this.congeToEdit
    );
    this.drawer.toggle();
  }

}
