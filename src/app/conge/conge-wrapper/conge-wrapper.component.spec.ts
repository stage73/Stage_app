import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CongeWrapperComponent } from './conge-wrapper.component';

describe('CongeWrapperComponent', () => {
  let component: CongeWrapperComponent;
  let fixture: ComponentFixture<CongeWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CongeWrapperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CongeWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
