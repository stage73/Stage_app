import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { Router } from '@angular/router';
import { DocumentService } from 'src/app/services/document.service';
import { DocumentCommunicationServiceService } from '../../../services/document-communication-service.service';
import { Document } from 'src/app/interface/Models/document';
import { NotificationServiceService } from '../../../services/notification-service.service';
@Component({
  selector: 'app-document-wrapper',
  templateUrl: './document-wrapper.component.html',
  styleUrls: ['./document-wrapper.component.css']
})
export class DocumentWrapperComponent implements OnInit {
  @ViewChild('drawer') public drawer! : MatDrawer ;


  document! : Document ;
  constructor(private router : Router,
    private documentCommunicationService : DocumentCommunicationServiceService,
    private documentService : DocumentService,
    private notificationService : NotificationServiceService
    ) { }

  ngOnInit(): void {
  }
  onEditClickDocument(element : any){
    this.document = element;
    this.documentCommunicationService.setEditDocument(element);
    this.drawer.toggle();
  }
  goBack(): void {
    this.router.navigate(["./"]);
  }
  onSubmitDocument(element:any){
    if(element?.id){
      this.documentService.updateUser(element as Document)
      .subscribe((editedDocument : Document)=>{
        this.documentCommunicationService.setEditedDocument(editedDocument);
        this.drawer.toggle();
        this.notificationService.openNotification("Document updated succefully", "ok");
      })
    }else{
      console.log("wow223", element);
      this.documentService.addDocument(element as Document)
      .subscribe((addedDocument : Document)=>{
        console.log("wow222", addedDocument);
        this.documentCommunicationService.setAddedDocument(addedDocument);
        this.drawer.toggle();
        this.notificationService.openNotification("Document added succefully", "ok");
      })
    }
  }
}
