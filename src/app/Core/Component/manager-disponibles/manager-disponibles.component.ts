import { LiveAnnouncer } from '@angular/cdk/a11y';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatDrawer } from '@angular/material/sidenav';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import {
  Departement,
  DepartementDetails,
} from '../../../interface/Models/departement';
import { User, UserToDepartment } from '../../../interface/Models/user';
import { DepartementService } from '../../../services/departement.service';
import { DepartementsCommunicationService } from '../../../services/departements-communication.service';
import { NotificationServiceService } from '../../../services/notification-service.service';
import { UserService } from '../../../services/user.service';
import { UsersCommunicationService } from '../../../services/users-communication.service';

@Component({
  selector: 'app-manager-disponibles',
  templateUrl: './manager-disponibles.component.html',
  styleUrls: ['./manager-disponibles.component.css'],
})
export class ManagerDisponiblesComponent implements OnInit {
  departementDet!: DepartementDetails;
  userTable: UserToDepartment[] = [];
  manager!: User;

  @ViewChild('drawerr') public drawer!: MatDrawer;
  @Input() receivedDepForManager!: DepartementDetails;
  @Output() onAffectManager = new EventEmitter<boolean>();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  // departementTable : Departement[] = []  ;

  public defaultValue: string = '';
  public TableRow: BehaviorSubject<string> = new BehaviorSubject<string>(
    this.defaultValue
  );

  constructor(
    private departementService: DepartementService,
    private service: UserService,
    private _liveAnnouncer: LiveAnnouncer,
    private usersCommunicationService: UsersCommunicationService,
    private router: Router,
    private departementsCommunicationService: DepartementsCommunicationService,
    private notificationService: NotificationServiceService
  ) {}

  ngOnInit(): void {
    this.getUsersDisponibles();
  }

  usersTable: User[] = [];
  dataSource: any = null;
  displayedColumns = ['firstName', 'lastName', 'update'];

  filterchange(event: Event) {
    const filvalue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filvalue;
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  getUsersDisponibles() {
    this.service.getManagerDisponibles().subscribe(
      (resp) => {
        this.usersTable = resp;
        this.dataSource = new MatTableDataSource(this.usersTable);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  affectManagerToDepartement(receivedDepForManager: any, user: User) {
    this.departementDet = receivedDepForManager;
    this.userTable = this.departementDet.users;
    if (this.userTable.length === 0) {
      this.usersCommunicationService.setAffectedManager(user);
      this.service.affectUserToDepartement(this.departementDet, user).subscribe(
        (resp) => {
          this.notificationService.openNotification(
            'Manager affected to departement successfully',
            'OK'
          );
          this.service.getManagerDisponibles().subscribe(
            (resp) => {
              this.usersTable = resp;
              this.dataSource = new MatTableDataSource(this.usersTable);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
            },
            (err) => {
              console.log(err);
            }
          );
        },
        (err) => {
          console.log(err);
        }
      );
    } else {
      this.usersCommunicationService.setAffectedUser(user);
      this.service.affectUserToDepartement(this.departementDet, user).subscribe(
        (resp) => {
          
          this.notificationService.openNotification(
            'Manager affected to departement successfully',
            'OK'
          );
          this.service.getManagerDisponibles().subscribe(
            (resp) => {
              this.usersTable = resp;
              this.dataSource = new MatTableDataSource(this.usersTable);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
            },
            (err) => {
              console.log(err);
            }
          );
        },
        (err) => {
          console.log(err);
        }
      );
    }

    this.onAffectManager.emit(true);
  }
}
