import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerDisponiblesComponent } from './manager-disponibles.component';

describe('ManagerDisponiblesComponent', () => {
  let component: ManagerDisponiblesComponent;
  let fixture: ComponentFixture<ManagerDisponiblesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagerDisponiblesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ManagerDisponiblesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
