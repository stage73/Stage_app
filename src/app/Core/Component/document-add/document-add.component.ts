import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DocumentCommunicationServiceService } from '../../../services/document-communication-service.service';
import { Patterns } from 'src/app/interface/Models/constants';
import {MatDatepicker} from '@angular/material/datepicker';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MomentDateAdapter, MAT_MOMENT_DATE_ADAPTER_OPTIONS} from '@angular/material-moment-adapter';
import { Document } from 'src/app/interface/Models/document';
import * as _moment from 'moment';
import {default as _rollupMoment, Moment} from 'moment';
const moment = _rollupMoment || _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'MM/YYYY',
  },
  display: {
    dateInput: 'MM/YYYY',
    monthYearLabel: 'MM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MM YYYY',
  },
};
@Component({
  selector: 'app-document-add',
  templateUrl: './document-add.component.html',
  styleUrls: ['./document-add.component.css'],
  providers: [
    // `MomentDateAdapter` can be automatically provided by importing `MomentDateModule` in your
    // application's root module. We provide it at the component level here, due to limitations of
    // our example generation script.
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },

    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class DocumentAddComponent implements OnInit {

  @Output() onCancelEvent = new EventEmitter<boolean>();
  @Output() onSubmitEvent = new EventEmitter<any>();
  date = new FormControl(moment());
  documentToEdit! : Document ;
  createDocumentForm! : FormGroup ;
  constructor(private builder : FormBuilder,
    private documentCommunicationService: DocumentCommunicationServiceService
    ) { }

  ngOnInit(): void {
    this.documentCommunicationService.getEditDocument().subscribe((document : Document)=> {
    this.documentToEdit = document ;
    this.createDocumentForm = this.builder.group({
      id:[this.documentToEdit.id],
      type:[this.documentToEdit.type,[Validators.required,]],
      dateDemande:this.date,
    });
    })
    if(!this.documentToEdit){
      this.createDocumentForm = this.builder.group({
        id:[null],
        type:['',[Validators.required,]],
        status : ["ENCOURS"],
        dateDemande: this.date ,
      })
    }
  }

  addDocument(){
    this.onSubmitEvent.emit(this.createDocumentForm.value);
    this.createDocumentForm.reset();
    console.log("hhe", this.createDocumentForm.value);
  }
  onCancel(){
    this.onCancelEvent.emit(true);
    this.createDocumentForm.reset();
  }

  setMonthAndYear(normalizedMonthAndYear: Moment, datepicker: MatDatepicker<Moment>) {
    const ctrlValue = this.date.value!;
    ctrlValue.month(normalizedMonthAndYear.month());
    ctrlValue.year(normalizedMonthAndYear.year());
    this.date.setValue(ctrlValue);
    datepicker.close();
  }
}
