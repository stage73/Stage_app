import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Patterns } from 'src/app/interface/Models/Patterns';
import { DepartementToEdit } from '../../../interface/Models/departement';
import { DepartementService } from '../../../services/departement.service';
import { DepartementsCommunicationService } from 'src/app/services/departements-communication.service';

@Component({
  selector: 'app-departement-form',
  templateUrl: './departement-form.component.html',
  styleUrls: ['./departement-form.component.css'],
})
export class DepartementFormComponent implements OnInit {
  @Output() onCancelEvent = new EventEmitter<boolean>();
  @Output() onSubmitEvent = new EventEmitter<any>();

  departementToEdit!: DepartementToEdit;
  createDepartementForm!: FormGroup;

  constructor(
    private builder: FormBuilder,
    private api: DepartementService,
    private departementsCommunicationService: DepartementsCommunicationService
  ) {}

  ngOnInit(): void {
    this.departementsCommunicationService
      .getEditDepartement()
      .subscribe((departementToEdit: any) => {
        this.departementToEdit = departementToEdit;
        this.createDepartementForm = this.builder.group({
          id: [this.departementToEdit.id],
          name: this.builder.control(this.departementToEdit.name, [
            Validators.required,
            Validators.pattern(Patterns.unamePattern),
          ]),
          place: this.builder.control(
            this.departementToEdit.place,
            Validators.required
          ),
        });
      });
    if (!this.departementToEdit) {
      this.createDepartementForm = this.builder.group({
        id: [null],
        name: this.builder.control('', [
          Validators.required,
          Validators.pattern(Patterns.unamePattern),
        ]),
        place: this.builder.control('', Validators.required),
      });
    }
  }

  addDepartement() {
    this.onSubmitEvent.emit(this.createDepartementForm.value);
    this.createDepartementForm.reset();
  }

  onCancel() {
    this.onCancelEvent.emit(true);
    this.createDepartementForm.reset();
  }
}
