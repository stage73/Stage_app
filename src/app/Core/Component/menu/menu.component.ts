import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements OnInit {
  constructor(private router: Router) {}

  @ViewChild('sidenav') public sidenav!: MatSidenav;

  ngOnInit(): void {}

  goToUser(): void {
    this.router.navigate(['./user-table']);
    this.sidenav.close();
  }

  goToDepartement(): void {
    this.router.navigate(['./departement']);
    this.sidenav.close();
  }

  goToDocuments():void {
    this.router.navigate(["./document"])
  }


}
