import { Component, OnInit } from '@angular/core';
import { Chart , registerables} from 'node_modules/chart.js';
import { UserService } from '../../../services/user.service';
import { userRole } from '../../../interface/Models/userRole';
import { DocumentService } from '../../../services/document.service';
Chart.register(...registerables);

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

  constructor(private userService : UserService, private documentService : DocumentService) { }

  documentTable : any ;
  userTable : any ;
  Rh : any[]=[];
  USER : any[]= [];
  MANAGER : any[]=[];
  APPROUVE : any[]=[]
  ENCOURS : any[]=[]
  ngOnInit(): void {
    this.userService.getUsers().subscribe((resp) => {
      this.userTable = resp;
        for(let i = 0 ; i<this.userTable.length ; i++){
            if(this.userTable[i].userRole == "USER"){
                this.USER.push(this.userTable[i]);
            }
            else if(this.userTable[i].userRole == "RH"){
              this.Rh.push(this.userTable[i]);
          }
          else{
            this.MANAGER.push(this.userTable[i]);
          }
      }
      let nbrRH = this.Rh.length ;
      let nbrMANAGER = this.MANAGER.length;
      let nbrUSER = this.USER.length
      this.EmployeeChart( nbrRH , nbrMANAGER  , nbrUSER );
    })

    this.documentService.getDocument().subscribe(resp => {
        this.documentTable = resp ;
      console.log("yaaaak", this.documentTable);
        for(let i = 0 ; i < this.documentTable.length; i++){
            if(this.documentTable[i].status == "APPROUVE"){
              this.APPROUVE.push(this.documentTable[i])
            }
            else{
              this.ENCOURS.push(this.documentTable[i])
            }
        }
        let nbrApprouve = this.APPROUVE.length;
        let nbrEncours = this.ENCOURS.length;
        this.demandeStatusChart(nbrApprouve,nbrEncours )
    })



  }

  EmployeeChart(Rh : number , MANAGER : number , USER : number){
    const ctx = document.getElementById('myChart');
    new Chart('employees', {
      type: 'pie',
      data: {
        labels: ['RH', 'MANAGER', 'USER'],
        datasets: [{
          label: '# of Employee',
          data: [ Rh, MANAGER, USER ],
          borderWidth: 1
        }]
      },
      options: {

      }
    });
  }


  demandeStatusChart(Approuve : number , Encours : number ){
    const ctx = document.getElementById('myChart');
    new Chart('statusDemande', {
      type: 'pie',
      data: {
        labels: [ 'Approuve' , 'En Cours'],
        datasets: [{
          label: '# Number :',
          data: [ Approuve , Encours  ],
          borderWidth: 1
        }]
      },
      options: {

      }
    });
  }
}
