import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { Observable } from 'rxjs';
import { User, UserToDepartment } from '../../../interface/Models/user';
import { DepartementService } from '../../../services/departement.service';
import { DepartementsCommunicationService } from '../../../services/departements-communication.service';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {
  DepartementDetails,
  Departement,
} from '../../../interface/Models/departement';
import { UsersCommunicationService } from '../../../services/users-communication.service';
import { ThemePalette } from '@angular/material/core';
import { UserService } from 'src/app/services/user.service';
import { NotificationServiceService } from 'src/app/services/notification-service.service';

@Component({
  selector: 'app-departement-details',
  templateUrl: './departement-details.component.html',
  styleUrls: ['./departement-details.component.css'],
})
export class DepartementDetailsComponent implements OnInit {
  @Output() onCancelEvent = new EventEmitter<boolean>();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  managerToDesaffect!: unknown;
  usersDepartement: UserToDepartment[] = [];
  departementDetails!: DepartementDetails;
  depDet!: DepartementDetails;
  departementTable: Departement[] = [];
  departement!: any;
  userAffected!: UserToDepartment;
  dataSource: any = null;
  managerName: any;
  displayedDetailColumns = [
    'id',
    'firstName',
    'lastName',
    'dob',
    'dateEntree',
    'dateSortie',
    'age',
    'email',
    'phoneNumber',
    'address',
    'userRole',
  ];

  constructor(
    private departementsCommunicationService: DepartementsCommunicationService,
    private departementService: DepartementService,
    private route: ActivatedRoute,
    private router: Router,
    private UsersCommunicationService: UsersCommunicationService,
    private userService: UserService,
    private notificationService: NotificationServiceService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      const departementId = params['id'];
      this.departementService
        .getDepartement(departementId)
        .subscribe((departementDetails) => {
          this.depDet = departementDetails;
          this.usersDepartement = departementDetails.users;
          this.UsersCommunicationService.getAffectedManager().subscribe(
            (user: any) => {
              this.managerName = user.firstName;
              this.managerToDesaffect = user;
            }
          );
          for (let i = 0; i < this.usersDepartement.length; i++) {
            if (this.usersDepartement[i].userRole == 'MANAGER') {
              this.managerName = this.usersDepartement[i].firstName;
              this.managerToDesaffect = this.usersDepartement[i] as unknown;
            }
          }
          this.UsersCommunicationService.getAffectedUser().subscribe(
            (userAffected: any) => {
              this.usersDepartement.push(userAffected);
              this.dataSource = new MatTableDataSource(this.usersDepartement);
            }
          );
          this.dataSource = new MatTableDataSource(this.usersDepartement);
        });
    });
  }

  desaffectManager(manager: any) {
    this.userService.desaffectManager(manager).subscribe((resp) => {});

    window.location.reload();
    this.notificationService.openNotification(
      'Manager desaffected successfully',
      'Ok'
    );
  }

  onCancel() {
    this.onCancelEvent.emit(true);
  }

  goBack(): void {
    this.router.navigate(['./departement']);
  }

  idSend() {
    this.route.params.subscribe((params: Params) => {
      const departementId = params['id'];
      this.departementService
        .getDepartement(departementId)
        .subscribe((details: DepartementDetails) => {
          this.departement = details;
        });
    });
  }
}
