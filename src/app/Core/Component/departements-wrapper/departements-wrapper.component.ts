import {
  Component,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import {
  Departement,
  DepartementToEdit,
} from '../../../interface/Models/departement';
import { DepartementService } from '../../../services/departement.service';
import { DepartementsCommunicationService } from '../../../services/departements-communication.service';
import { NotificationServiceService } from '../../../services/notification-service.service';

@Component({
  selector: 'app-departements-wrapper',
  templateUrl: './departements-wrapper.component.html',
  styleUrls: ['./departements-wrapper.component.css'],
})
export class DepartementsWrapperComponent implements OnInit {
  departementToEdit: any;

  @ViewChild('drawer') public drawer!: MatDrawer;
  @ViewChild('drawerr') public drawerr!: MatDrawer;
  @Output() onDetailsEvent = new EventEmitter<boolean>();
  @Output() onCancelEvent = new EventEmitter<boolean>();

  constructor(
    private departementService: DepartementService,
    private departementsCommunicationService: DepartementsCommunicationService,
    private notificationService: NotificationServiceService
  ) {}

  ngOnInit(): void {}

  onSubmitDepartement($event: any) {
    if ($event?.id) {
      this.departementService
        .updateDepartement($event as DepartementToEdit)
        .subscribe((editedDepartement: Departement) => {
          this.departementsCommunicationService.setEditedDepartement(
            editedDepartement
          );
          this.drawer.toggle();
          this.notificationService.openNotification(
            'Departement updated succefully',
            'OK'
          );
        });
    } else {
      this.departementService
        .postDepartement($event as DepartementToEdit)
        .subscribe((addedDepartement: Departement) => {
          this.departementsCommunicationService.setAddedDepartement(
            addedDepartement
          );
          this.drawer.toggle();
          this.notificationService.openNotification(
            'Departement added succefully',
            'OK'
          );
        });
    }
  }

  onEditClick(element: any) {
    this.departementToEdit = element;
    this.departementsCommunicationService.setEditDepartement(
      this.departementToEdit
    );
    this.drawer.toggle();
  }

  onDetails(element: any) {
    this.departementToEdit = element;
    this.departementsCommunicationService.setDetailDepartement(
      this.departementToEdit
    );
    this.drawerr.toggle();
  }
}
