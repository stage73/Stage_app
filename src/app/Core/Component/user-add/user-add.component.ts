import { Component, Inject, Injectable, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Patterns } from 'src/app/interface/Models/constants';
import { UserService } from 'src/app/services/user.service';
import { Output, EventEmitter } from '@angular/core';
import { userToEdit } from '../../../interface/Models/user';
import { UsersCommunicationService } from '../../../services/users-communication.service';
@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css'],
})
export class UserAddComponent implements OnInit {
  @Output() onCancelEvent = new EventEmitter<boolean>();
  @Output() onSubmitEvent = new EventEmitter<any>();

  userToEdit!: userToEdit;

  createUserForm!: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private usersCommunicationService: UsersCommunicationService
  ) {}

  ngOnInit(): void {
    this.usersCommunicationService
      .getEditUser()
      .subscribe((userToEdit: any) => {
        this.userToEdit = userToEdit;
        this.createUserForm = this.formBuilder.group({
          id: [this.userToEdit.id],
          firstName: [
            this.userToEdit.firstName,
            [Validators.required, Validators.pattern(Patterns.namePattern)],
          ],
          lastName: [
            this.userToEdit.lastName,
            [Validators.required, Validators.pattern(Patterns.namePattern)],
          ],
          dob: [
            new Date(Date.parse(this.userToEdit.dob)),
            [Validators.required],
          ],
          dateEntree: [this.userToEdit.dateEntree, [Validators.required]],
          dateSortie: [this.userToEdit.dateSortie, [Validators.required]],
          age: [this.userToEdit.age, [Validators.required]],
          email: [
            this.userToEdit.email,
            [Validators.required, Validators.email],
          ],
          address: [this.userToEdit.address, Validators.required],
          userRole: [this.userToEdit.userRole, [Validators.required]],
          phoneNumber: [
            this.userToEdit.phoneNumber,
            [Validators.required, Validators.pattern(Patterns.phonePattern)],
          ],
        });
      });
    if (!this.userToEdit) {
      this.createUserForm = this.formBuilder.group({
        id: [null],
        firstName: [
          '',
          [Validators.required, Validators.pattern(Patterns.namePattern)],
        ],
        lastName: [
          '',
          [Validators.required, Validators.pattern(Patterns.namePattern)],
        ],
        dob: ['', Validators.required],
        dateEntree: ['', Validators.required],
        dateSortie: ['', Validators.required],
        age: ['', [Validators.required]],
        email: ['', [Validators.required, Validators.email]],
        address: ['', Validators.required],
        userRole: ['', Validators.required],
        phoneNumber: [
          '',
          [Validators.required, Validators.pattern(Patterns.phonePattern)],
        ],
      });
    }
  }

  addUser() {
    this.onSubmitEvent.emit(this.createUserForm.value);
    this.createUserForm.reset();
  }

  onCancel() {
    this.onCancelEvent.emit(true);
    this.createUserForm.reset();
  }
}
