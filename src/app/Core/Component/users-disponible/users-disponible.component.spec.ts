import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersDisponibleComponent } from './users-disponible.component';

describe('UsersDisponibleComponent', () => {
  let component: UsersDisponibleComponent;
  let fixture: ComponentFixture<UsersDisponibleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsersDisponibleComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UsersDisponibleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
