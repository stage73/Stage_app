import { LiveAnnouncer } from '@angular/cdk/a11y';
import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import {
  Departement,
  DepartementDetails,
} from 'src/app/interface/Models/departement';
import { DepartementsCommunicationService } from 'src/app/services/departements-communication.service';
import { UserService } from 'src/app/services/user.service';
import { UsersCommunicationService } from 'src/app/services/users-communication.service';
import {
  User,
  UserToDepartment,
  userToEdit,
} from '../../../interface/Models/user';
import { DepartementToEdit } from '../../../interface/Models/departement';
import { DepartementService } from '../../../services/departement.service';
import { NotificationServiceService } from '../../../services/notification-service.service';
@Component({
  selector: 'app-users-disponible',
  templateUrl: './users-disponible.component.html',
  styleUrls: ['./users-disponible.component.css'],
})
export class UsersDisponibleComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  @Input() receivedDep!: DepartementDetails;
  departementTable: Departement[] = [];

  public defaultValue: string = '';
  public TableRow: BehaviorSubject<string> = new BehaviorSubject<string>(
    this.defaultValue
  );

  constructor(
    private departementService: DepartementService,
    private service: UserService,
    private _liveAnnouncer: LiveAnnouncer,
    private usersCommunicationService: UsersCommunicationService,
    private router: Router,
    private departementsCommunicationService: DepartementsCommunicationService,
    private notificationService: NotificationServiceService
  ) {
    this.getUsersDisponibles();
  }

  departementDetails!: DepartementDetails;
  ngOnInit(): void {}

  usersTable: User[] = [];
  dataSource: any = null;
  displayedColumns = ['firstName', 'lastName', 'update'];

  filterchange(event: Event) {
    const filvalue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filvalue;
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }

  getUsersDisponibles() {
    this.service.getUsersDisponibles().subscribe(
      (resp) => {
        
        this.usersTable = resp;
        this.dataSource = new MatTableDataSource(this.usersTable);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  affectUserToDepartement(receivedDep: any, user: User) {
    this.departementDetails = receivedDep;
    this.usersCommunicationService.setAffectedUser(user);
    this.service
      .affectUserToDepartement(this.departementDetails, user)
      .subscribe(
        (resp) => {
          
          this.notificationService.openNotification(
            'User affected to departement successfully',
            'OK'
          );
          this.service.getUsersDisponibles().subscribe(
            (resp) => {
              
              this.usersTable = resp;
              this.dataSource = new MatTableDataSource(this.usersTable);
              this.dataSource.paginator = this.paginator;
              this.dataSource.sort = this.sort;
            },
            (err) => {
              console.log(err);
            }
          );
        },
        (err) => {
          console.log(err);
        }
      );
  }
}
