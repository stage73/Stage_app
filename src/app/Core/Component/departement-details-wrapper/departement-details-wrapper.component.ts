import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';

@Component({
  selector: 'app-departement-details-wrapper',
  templateUrl: './departement-details-wrapper.component.html',
  styleUrls: ['./departement-details-wrapper.component.css']
})
export class DepartementDetailsWrapperComponent implements OnInit {

  @ViewChild('drawer') public drawer!: MatDrawer;
  constructor() { }

  ngOnInit(): void {
  }

}
