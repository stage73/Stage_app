import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartementDetailsWrapperComponent } from './departement-details-wrapper.component';

describe('DepartementDetailsWrapperComponent', () => {
  let component: DepartementDetailsWrapperComponent;
  let fixture: ComponentFixture<DepartementDetailsWrapperComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DepartementDetailsWrapperComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DepartementDetailsWrapperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
