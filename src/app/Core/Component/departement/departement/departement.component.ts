import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { DepartementService } from 'src/app/services/departement.service';
import { DepartementFormComponent } from 'src/app/Core/Component/departement-add/departement-form.component';
import { DepartementsCommunicationService } from 'src/app/services/departements-communication.service';
import { Departement } from 'src/app/interface/Models/departement';
import { MatDrawer } from '@angular/material/sidenav';
import { Router } from '@angular/router';


@Component({
  selector: 'app-departement',
  templateUrl: './departement.component.html',
  styleUrls: ['./departement.component.css']
})
export class DepartementComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'place', 'update', 'delete', 'details'];
  dataSource: any = null;
  departementsTable: Departement[] = [];
  departementToEdit: any


  @Output() onDepartementDetails = new EventEmitter<any>();
  @Output() onClickEdit = new EventEmitter<any>();
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private service: DepartementService,
    private departementsCommunicationService: DepartementsCommunicationService, private router: Router
  ) {
    this.GetAllDepartement();
  }

  ngOnInit(): void {
    this.GetAllDepartement();
    this.departementsCommunicationService.getAddedDepartement().subscribe((addedDepartement : any) => {
      this.departementsTable.push(addedDepartement);
      this.dataSource = new MatTableDataSource(this.departementsTable);
    });
    this.departementsCommunicationService.getEditDepartement().subscribe((editDepartement : any) => {
      let index = this.departementsTable.findIndex(departement => departement.id === editDepartement.id);
      this.departementsTable[index] = editDepartement;
      this.dataSource = new MatTableDataSource(this.departementsTable);
    })
  }

  FilterChange(event: Event) {
    const filvalue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filvalue;
  }

  onClick(element: any) {
    this.onClickEdit.emit(this.departementsTable.filter(departement => departement.id == element.id)[0]);
  }


  GetAllDepartement() {
    this.service.getDepartements().subscribe(
      (resp) => {
        this.departementsTable = resp;
        this.dataSource = new MatTableDataSource(this.departementsTable);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {
        console.log(err);
      }
    )
  }

  onDeleteUser(id: number) {
    this.service.deleteDepartement(id).subscribe();
  }

  goToDetails(element: any) {
    this.router.navigateByUrl(`departement-details/${element.id}`);
    this.departementToEdit = element;
    this.departementsCommunicationService.setDetailDepartement(this.departementToEdit);
  }
}

