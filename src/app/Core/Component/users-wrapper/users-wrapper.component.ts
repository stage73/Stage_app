import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { UserService } from 'src/app/services/user.service';
import { User, userToEdit } from '../../../interface/Models/user';
import { UserAddComponent } from '../user-add/user-add.component';
import { UserTableComponent } from '../user-table/user-table.component';
import { UsersCommunicationService } from '../../../services/users-communication.service';
import { NotificationServiceService } from '../../../services/notification-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-wrapper',
  templateUrl: './users-wrapper.component.html',
  styleUrls: ['./users-wrapper.component.css'],
})
export class UsersWrapperComponent implements OnInit {
  @ViewChild('drawer') public drawer!: MatDrawer;

  constructor(
    private userService: UserService,
    private UserAddComponent: UserAddComponent,
    private userTable: UserTableComponent,
    private usersCommunicationService: UsersCommunicationService,
    private notificationService: NotificationServiceService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  userToEdit: any;
  onSubmitUser($event: any) {
    if ($event?.id) {
      this.userService
        .updateUser($event as userToEdit)
        .subscribe((editedUser: User) => {
          this.usersCommunicationService.setEditedUser(editedUser);
          this.drawer.toggle();
          this.notificationService.openNotification(
            'User updated succefully',
            'OK'
          );
        });
    } else {
      this.userService
        .postUser($event as userToEdit)
        .subscribe((addedUser: User) => {
          this.usersCommunicationService.setAddedUser(addedUser);
          this.drawer.toggle();
          this.notificationService.openNotification(
            'User added succefully',
            'OK'
          );
        });
    }
  }

  onEditClick(element: any) {
    this.userToEdit = element;
    this.usersCommunicationService.setEditUser(this.userToEdit);
    this.drawer.toggle();
  }

  goBack(): void {
    this.router.navigate(['./']);
  }
}
