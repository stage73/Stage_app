import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSidenav } from '@angular/material/sidenav';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from 'src/app/services/user.service';
import { Output, EventEmitter } from '@angular/core';
import { User, userToEdit } from '../../../interface/Models/user';
import { BehaviorSubject } from 'rxjs';
import { UsersCommunicationService } from '../../../services/users-communication.service';
import { Router } from '@angular/router';
import { NotificationServiceService } from 'src/app/services/notification-service.service';
import { userRole } from 'src/app/interface/Models/userRole';
@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css'],
})
export class UserTableComponent implements OnInit {
  @Output() onClickEdit = new EventEmitter<any>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public defaultValue: string = '';
  public TableRow: BehaviorSubject<string> = new BehaviorSubject<string>(
    this.defaultValue
  );

  usersTable: User[] = [];
  dataSource: any = null;
  displayedColumns = [
    'firstName',
    'lastName',
    'dob',
    'dateEntree',
    'dateSortie',
    'age',
    'email',
    'phoneNumber',
    'address',
    'userRole',
    'update',
    'delete',
  ];

  constructor(
    private service: UserService,
    private _liveAnnouncer: LiveAnnouncer,
    private usersCommunicationService: UsersCommunicationService,
    private router: Router,
    private notificationService: NotificationServiceService
  ) {
    this.getUsers();
  }

  ngOnInit(): void {
    this.getUsers();
    this.usersCommunicationService
      .getAddedUser()
      .subscribe((addedUser: User) => {
        this.usersTable.push(addedUser);
        this.dataSource = new MatTableDataSource(this.usersTable);
      });

    this.usersCommunicationService
      .getEditedUser()
      .subscribe((editedUser: User) => {
        let index = this.usersTable.findIndex(
          (user) => user.id == editedUser.id
        );
        this.usersTable[index] = editedUser;
        this.dataSource = new MatTableDataSource(this.usersTable);
      });
  }
  onClick(element: any) {
    this.onClickEdit.emit(
      this.usersTable.filter((user) => user.id == element.id)[0]
    );
  }

  getUsers() {
    this.service.getUsers().subscribe(
      (resp) => {
        
        this.usersTable = resp;
        this.dataSource = new MatTableDataSource(this.usersTable);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  onDeleteUser(id: number) {
    this.service.deleteUser(id).subscribe((resp) => {
      this.getUsers();
      this.notificationService.openNotification(
        'User deleted successfully',
        'Ok'
      );
    });
  }

  filterchange(event: Event) {
    const filvalue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filvalue;
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
}
