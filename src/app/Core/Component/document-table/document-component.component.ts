import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { DocumentService } from 'src/app/services/document.service';
import { Document } from 'src/app/interface/Models/document';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { DocumentCommunicationServiceService } from '../../../services/document-communication-service.service';
import { NotificationServiceService } from '../../../services/notification-service.service';
@Component({
  selector: 'app-document-component',
  templateUrl: './document-component.component.html',
  styleUrls: ['./document-component.component.css']
})
export class DocumentComponentComponent implements OnInit {
  @Output() onClickEditDocument = new EventEmitter<any>();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  now = new Date();
  Encours : String = "En cours"
  documentTable!: Document[];
  dataSource : any = null ;
  displayedColumns = [ 'type' , 'dateDemande' ,'status' , 'update' , 'delete'];
  constructor(
    private _liveAnnouncer: LiveAnnouncer,
    private service : DocumentService,
    private documentCommunicationServiceService : DocumentCommunicationServiceService,
    private notificationServiceService : NotificationServiceService
    ) { }

  ngOnInit(): void {
    this.getDocuments();
    this.documentCommunicationServiceService.getAddedDocument()
    .subscribe((document : Document)=>{
        this.documentTable.push(document);
        this.dataSource = new MatTableDataSource(this.documentTable);
    });
    this.documentCommunicationServiceService.getEditedDocument()
    .subscribe((documentEdited : Document)=>{
      let index = this.documentTable.findIndex(
        (document) => document.id == documentEdited.id
      );
        this.documentTable[index] = documentEdited;
        this.dataSource = new MatTableDataSource(this.documentTable);
    });
  }

  update(element : any){
      this.onClickEditDocument.emit(
      this.documentTable.filter((document)=> document.id == element.id)[0]
      )
    }

  onDeleteDocument(id : number){
    this.service.deleteDocument(id).subscribe(
      (resp : any)=>{
        this.getDocuments();
        this.notificationServiceService.openNotification("Documenet deleted succefully", "OK");
      }
    )
  }

  getDocuments(){
    this.service.getDocument().subscribe(
      (resp:any)=> {
      this.documentTable = resp;
      this.dataSource = new MatTableDataSource(this.documentTable);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }

    )
  }
  filterchange(event: Event) {
    const filvalue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filvalue;
  }

  announceSortChange(sortState: Sort) {
    if (sortState.direction) {
      this._liveAnnouncer.announce(`Sorted ${sortState.direction}ending`);
    } else {
      this._liveAnnouncer.announce('Sorting cleared');
    }
  }
}
