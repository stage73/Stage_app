import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router'


  @Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent  {
  title = 'stage_App';
  opened = false ;

  constructor( private router:Router){}


    goToPage():void{
       this.router.navigate(["./user-table"]);
    }


}
