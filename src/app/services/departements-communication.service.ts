import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Departement, DepartementDetails } from '../interface/Models/departement';

@Injectable({
  providedIn: 'root'
})
export class DepartementsCommunicationService {

  private savedDepartementSource = new Subject<Departement>();
  private savedUser$ = this.savedDepartementSource.asObservable();

  private editDepartementSource= new Subject<Departement>();
  private editDepartement$ = this.editDepartementSource.asObservable();

  private editedDepartementSource= new Subject<Departement>();
  private editedDepartement$ = this.editedDepartementSource.asObservable();

  private detailDepartementSource= new Subject<DepartementDetails>();
  private detailDepartement$ = this.detailDepartementSource.asObservable();

 


  constructor() { }

  setDetailDepartement(departementToEdit: DepartementDetails) :void{
    this.detailDepartementSource.next(departementToEdit);
  }

  getDetailDepartement():Observable<DepartementDetails>{
    return this.detailDepartement$;
  }

  setEditedDepartement(departementToEdit: Departement):void{
    this.editedDepartementSource.next(departementToEdit);
  }

  getEditedDepartement():Observable<Departement>{
    return this.editedDepartement$ ;
  }


  setEditDepartement(departementToEdit: Departement):void{
    this.editDepartementSource.next(departementToEdit);
  }

  getEditDepartement():Observable<Departement>{
    return this.editDepartement$ ;
  }


  setAddedDepartement(addedDepartement: Departement):void {
    this.savedDepartementSource.next(addedDepartement);
  }

  getAddedDepartement():Observable<Departement> {
    return this.savedUser$;
  }
}
