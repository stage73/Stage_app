import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { User, userToEdit } from '../interface/Models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersCommunicationService {

  private savedUserSource = new Subject<User>();
  private savedUser$ = this.savedUserSource.asObservable();

  private editUserSource= new Subject<User>();
  private editUser$ = this.editUserSource.asObservable();

  private editedUserSource= new Subject<User>();
  private editedUser$ = this.editedUserSource.asObservable();

  private affectedUserSource= new Subject<User>();
  private affectedUser$= this.affectedUserSource.asObservable();

  private affectedManagerSource = new Subject<User>();
  private affectedManager$ = this.affectedManagerSource.asObservable();

  constructor() { }

  setAffectedManager(userToDepartment: User):void{
    this.affectedManagerSource.next(userToDepartment);
  }
  getAffectedManager():Observable<User>{
    return this.affectedManager$ ;
  }

  setAffectedUser(userToDepartment: User):void{
    this.affectedUserSource.next(userToDepartment);
  }

  getAffectedUser():Observable<User>{
    return this.affectedUser$ ;
  }

  setEditedUser(userToEdit: User):void{
    this.editedUserSource.next(userToEdit);
  }

  getEditedUser():Observable<User>{
    return this.editedUser$ ;
  }

  setEditUser(userToEdit: User):void{
    this.editUserSource.next(userToEdit);
  }

  getEditUser():Observable<User>{
    return this.editUser$ ;
  }

  setAddedUser(addedUser: User): void{
    this.savedUserSource.next(addedUser);
  }

  getAddedUser(): Observable<User> {
    return this.savedUser$;
  }
}
