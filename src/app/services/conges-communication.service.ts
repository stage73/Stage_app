import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { Conge, CongeToAdd, CongeToEdit } from '../interface/conge';

@Injectable({
  providedIn: 'root',
})
export class CongesCommunicationService {
  private savedCongeSource = new Subject<CongeToAdd>();
  private savedConge$ = this.savedCongeSource.asObservable();

  private editCongeSource = new Subject<CongeToEdit>();
  private editConge$ = this.editCongeSource.asObservable();

  private editedCongeSource = new Subject<Conge>();
  private editedConge$ = this.editedCongeSource.asObservable();

  constructor() {}

  setAddedConge(addedConge: CongeToAdd): void {
    this.savedCongeSource.next(addedConge);
  }

  getAddedConge(): Observable<CongeToAdd> {
    return this.savedConge$;
  }

  setEditConge(congeToEdit: CongeToEdit) {
    this.editCongeSource.next(congeToEdit);
  }

  getEditConge(): Observable<CongeToEdit> {
    return this.editConge$;
  }

  setEditedConge(congeToEdit: Conge) {
    this.editedCongeSource.next(congeToEdit);
  }

  getEditedConge(): Observable<Conge> {
    return this.editedConge$;
  }
}
