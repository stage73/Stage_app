import { TestBed } from '@angular/core/testing';

import { CongesCommunicationService } from './conges-communication.service';

describe('CongesCommunicationService', () => {
  let service: CongesCommunicationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CongesCommunicationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
