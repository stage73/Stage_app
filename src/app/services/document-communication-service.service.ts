import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { Document } from '../interface/Models/document';
@Injectable({
  providedIn: 'root'
})
export class DocumentCommunicationServiceService {

  private editDocumentSource = new Subject<Document>();
  private editDocument$ = this.editDocumentSource.asObservable();

  private editedDocumentSource = new Subject<Document>();
  private editedDocument$ = this.editedDocumentSource.asObservable();

  private addedDocumentSource = new Subject<Document>();
  private addedDocument$ = this.addedDocumentSource.asObservable();
  constructor() { }

  setEditDocument(document : Document):void{
    this.editDocumentSource.next(document);
  }
  getEditDocument():Observable<Document>{
    return this.editDocument$ ;
  }

  setEditedDocument(document : Document):void{
    this.editedDocumentSource.next(document);
  }
  getEditedDocument():Observable<Document>{
    return this.editedDocument$ ;
  }

  setAddedDocument(document : Document):void{
    this.addedDocumentSource.next(document);
  }
  getAddedDocument():Observable<Document>{
    return this.addedDocument$ ;
  }
}
