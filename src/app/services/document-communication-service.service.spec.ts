import { TestBed } from '@angular/core/testing';

import { DocumentCommunicationServiceService } from './document-communication-service.service';

describe('DocumentCommunicationServiceService', () => {
  let service: DocumentCommunicationServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocumentCommunicationServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
