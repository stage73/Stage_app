import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Conge, CongeToAdd, CongeToEdit, STATUS } from '../interface/conge';

@Injectable({
  providedIn: 'root',
})
export class CongeService {
  constructor(private http: HttpClient) {}

  getConges(): Observable<Conge[]> {
    return this.http.get<Conge[]>(environment.apiUrl + 'conges/');
  }

  public postConge(conge: CongeToAdd): Observable<CongeToAdd> {
    return this.http.post<CongeToAdd>(
      environment.apiUrl + 'conge',
      conge
    ) as Observable<CongeToAdd>;
  }

  public updateConge(conge: CongeToEdit): Observable<Conge> {
    return this.http.put<Conge>(
      `${environment.apiUrl}conge/${conge.id}`,
      conge
    );
  }

  public deleteConge(id: number) {
    return this.http.delete(`${environment.apiUrl}conge/${id}`);
  }
}
