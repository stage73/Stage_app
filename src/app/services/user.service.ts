import { Injectable } from '@angular/core';
import {HttpClientModule } from '@angular/common/http';
import {HttpClient} from "@angular/common/http";
import { environment } from 'src/environments/environment';
import { User, UserToDepartment, userToEdit } from '../interface/Models/user';
import { catchError, Observable } from 'rxjs';
import { Departement, DepartementDetails } from '../interface/Models/departement';


@Injectable({
  providedIn: 'root'
})
export class UserService {


  constructor(private http : HttpClient) { }


  public postUser(user:userToEdit): Observable<User> {
    return this.http.post<User>(environment.apiUrl+'user/', user) as Observable<User>;
  }

  getUsers() : Observable<User[]> {
    return this.http.get<User[]>(environment.apiUrl+'user/');
  }

  getUser() : Observable<User> {
    return this.http.get<User>(environment.apiUrl+'user/1');
  }

  public deleteUser(id:number){
    return this.http.delete(`${environment.apiUrl}user/${id}`);
  }

  public updateUser(user:userToEdit): Observable<User>{
    return this.http.put<User>(`${environment.apiUrl}user/${user.id}`,user);
  }

  getUsersDisponibles() : Observable<User[]> {
    return this.http.get<User[]>(environment.apiUrl+'user/disponibles');
  }

  public affectUserToDepartement( departement:DepartementDetails , user:User ) {
    return this.http.put(`${environment.apiUrl}departement/${departement.id}/user/${user.id}`, {} );
  }

  getManagerDisponibles() : Observable<User[]> {
    return this.http.get<User[]>(environment.apiUrl+'user/manager/disponibles');
  }

  desaffectManager(user:User): Observable<User>{
    return this.http.patch<User>(`${environment.apiUrl}user/desaffecte/${user.id}`,user);
  }
}
