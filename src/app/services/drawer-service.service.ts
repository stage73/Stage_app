import { Injectable } from '@angular/core';
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class DrawerService {

  constructor() { }

  private _drawerDepartmentDetails = new BehaviorSubject(false);
  private _drawerDepartementForm = new BehaviorSubject(false);

  drawerDepartmentDetails$ = this._drawerDepartmentDetails.asObservable();
  drawerDepartementForm$ = this._drawerDepartementForm.asObservable();

  set drawerDepartmentDetails(v: boolean) {
    this._drawerDepartmentDetails.next(v);
  }

  get drawerDepartmentDetails(): boolean {
    return this._drawerDepartmentDetails.value;
  }

  set drawerDepartementForm(v: boolean) {
    this._drawerDepartementForm.next(v);
  }

  get drawerDepartementForm(): boolean {
    return this._drawerDepartementForm.value;
  }

}
