import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Departement, DepartementDetails, DepartementToEdit } from '../interface/Models/departement';
import { environment } from 'src/environments/environment';
import { User, UserToDepartment } from '../interface/Models/user';

@Injectable({
  providedIn: 'root'
})
export class DepartementService {


  constructor(private http: HttpClient) {

  }

  public getDepartements(): Observable<Departement[]> {
    return this.http.get<Departement[]>(environment.apiUrl + 'departements');
  }

  public getDepartement(departementId: number): Observable<DepartementDetails> {
    return this.http.get<DepartementDetails>(environment.apiUrl + 'departement/' + departementId);
  }

  public postDepartement(departement:DepartementToEdit): Observable<Departement> {
    return this.http.post<Departement>(environment.apiUrl + 'departement', departement) as Observable<Departement>;
  }

  public updateDepartement(departement:DepartementToEdit): Observable<Departement> {
    return this.http.put<Departement>(`${environment.apiUrl + 'departement'}/${departement.id}`,departement);
  }

  public deleteDepartement(id:number){
    return this.http.delete(`${environment.apiUrl + 'departement'}/${id}`);
  }

  public getUsersByDepartement(id:number){
    return this.http.get<UserToDepartment[]>(`${environment.apiUrl + 'departement'}/${id}/user`);
  }

}
