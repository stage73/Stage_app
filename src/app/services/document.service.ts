import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Document } from '../interface/Models/document';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class DocumentService {

  constructor(private http : HttpClient) { }

  public getDocument():Observable<Document[]>{
    return this.http.get<Document[]>(environment.apiUrl+'demande');
  }
  public updateUser(document:Document): Observable<Document>{
    return this.http.put<Document>(`${environment.apiUrl}demande/${document.id}`,document);
  }
  public addDocument(document : Document): Observable<Document>{
    return this.http.post<Document>(environment.apiUrl+'demande', document) as Observable<Document>;
  }
  public deleteDocument(id : number){
    return this.http.delete(`${environment.apiUrl}demande/${id}`)
  }
}

