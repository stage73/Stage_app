import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { FormsModule } from '@angular/forms';
import { UserAddComponent } from './Core/Component/user-add/user-add.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MenuComponent } from './Core/Component/menu/menu.component';
import { MatMenuModule } from '@angular/material/menu';
import { MaterialModule } from './material-module';
import { DepartementComponent } from './Core/Component/departement/departement/departement.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { DepartementFormComponent } from './Core/Component/departement-add/departement-form.component';
import { UserTableComponent } from './Core/Component/user-table/user-table.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import {MatCardModule} from '@angular/material/card';
import { ManagerDisponiblesComponent } from './Core/Component/manager-disponibles/manager-disponibles.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { UsersWrapperComponent } from './Core/Component/users-wrapper/users-wrapper.component';
import { DepartementDetailsWrapperComponent } from './Core/Component/departement-details-wrapper/departement-details-wrapper.component';
import { UsersDisponibleComponent } from './Core/Component/users-disponible/users-disponible.component';
import { DepartementDetailsComponent } from './Core/Component/departement-details/departement-details.component';
import { DepartementsWrapperComponent } from './Core/Component/departements-wrapper/departements-wrapper.component';
import { CongeFormComponent } from './conge/conge-form/conge-form.component';
import { CongeTableComponent } from './conge/conge-table/conge-table.component';
import { CongeWrapperComponent } from './conge/conge-wrapper/conge-wrapper.component';
import { DatePipe } from '@angular/common';



@NgModule({
  declarations: [
    AppComponent,
    UsersWrapperComponent,
    UserAddComponent,
    MenuComponent,
    DepartementComponent,
    DepartementFormComponent,
    UserTableComponent,
    DepartementsWrapperComponent,
    DepartementDetailsComponent,
    UsersDisponibleComponent,
    DepartementDetailsWrapperComponent,
    ManagerDisponiblesComponent,
    CongeTableComponent,
    CongeFormComponent,
    CongeWrapperComponent,


  ],
  imports: [
    MatCardModule,
    MatSnackBarModule,
    MatDialogModule,
    MatMenuModule,
    ReactiveFormsModule,
    FormsModule,
    MatListModule,
    MatIconModule,
    MatSidenavModule,
    MatToolbarModule,
    BrowserModule,
    AppRoutingModule,
    MatSlideToggleModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MaterialModule,
    MatButtonModule,
    HttpClientModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCheckboxModule,
    MatDialogModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: 'user-table', component: UserTableComponent },
    ]),
  ],
  providers: [
    UserAddComponent,
    UserTableComponent,
    DepartementComponent,
    DatePipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
